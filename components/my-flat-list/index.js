import React from 'react';
import { FlatList, Text, View,StyleSheet, Image } from 'react-native';
import {styles} from './style';

const MyFlatList = ({data}) =>(
    <View style={styles.container}>
        <FlatList
        style={styles.flatList}
        data={data}
        renderItem={({item}) => 
        <View style={styles.listLine}>
            <Image
            source={require('../../assets/img/avatar.png')}
            style={{width: 50, height: 50}}
            />
            <Text style={item.paye == 1 ? styles.normalText : styles.redText }>{item.nom}, {item.prenom}</Text>
            </View>
        }
        keyExtractor={(item, index) => item.id}
        />      
    </View>
            
);



  export default MyFlatList;
