import {StyleSheet} from 'react-native'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop:20,
      },
      flatList: {
        flex: 1,
      },
      listLine: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#fff',
      },
      normalText: {
        fontSize: 20,
      },
      redText: {
        fontSize: 20,
        color: '#D02C2C',
      },
});