import React from 'react';
import { Text, View,StyleSheet, Image } from 'react-native';
import {styles} from './style';
import { FormLabel, FormInput, FormValidationMessage,Button } from 'react-native-elements'
import t from 'tcomb-form-native'; // 0.6.11 

// Form
var Form = t.form.Form;

// Form model
var User = t.struct({
  login: t.String,
  password: t.String,
});
var options = {}; // optional rendering options (see documentation)

const Login = ({handleSubmit}) =>(
    <View style={styles.container}> 
        <Form 
          ref="form"
          type={User} 
          options={options}
          />
        <Button
          title="Sign Up!"
          onPress={()=> console.log(this.refs.form.getValue())}
        />
    </View>
            
);



  export default Login;
