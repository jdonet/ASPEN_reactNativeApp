import React from 'react';
import {View,StyleSheet, Picker,Text,ActivityIndicator,NetInfo,AsyncStorage,Button } from 'react-native';
import MyFlatList  from './components/my-flat-list';
import Login  from './components/login';
import t from 'tcomb-form-native'; // 0.6.11 

// Form
var Form = t.form.Form;

// Form model
var User = t.struct({
  login: t.String,
  password: t.String,
});
var options = {}; // optional rendering options (see documentation)


export default class App extends React.Component {

  constructor(props){
    super(props);
    this.state ={ 
      isLoading: true,
      section:'toutes',
      adherents:{},
      adherentsFiltres:{},
      logged:false,
    }
  }
  /**
   * Check network state
   */
  checkNetWork = () => {
 
  NetInfo.getConnectionInfo().then((connectionInfo) => {
    //console.log('Initial, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);
    if (connectionInfo.type === "none" || connectionInfo.type ==="unknow"){
      this.setState({
        networkPresent: false,
      })
    }else{
      this.setState({
        networkPresent: true,
      })
    }
  });
  function handleFirstConnectivityChange(connectionInfo) {
    //console.log('First change, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);
    NetInfo.removeEventListener(
      'connectionChange',
      handleFirstConnectivityChange
    );
  }
  NetInfo.addEventListener(
    'connectionChange',
    handleFirstConnectivityChange
  );
}



//persistance
_storeData = async () => {
  try {
    await AsyncStorage.setItem('@Jdonet:ASPEN', JSON.stringify(this.state.adherents));
    console.log('saving OK');
  } catch (error) {
    // Error saving data
    console.log("Error saving data")
  }
}

_retrieveData = async () => {
  try {
    const allAdhs = await AsyncStorage.getItem('@Jdonet:ASPEN');
    if (allAdhs !== null) {
      // We have data!!
      this.setState({
        isLoading: false,
        adherents: JSON.parse(allAdhs),
        adherentsFiltres: JSON.parse(allAdhs),
      })
    }
    console.log('retrieving OK');

   } catch (error) {
     // Error retrieving data
     console.log('Error retrieving data');
   }
}
//fin persistance


//appelé au lancement de l'application
componentDidMount(){
  this.checkNetWork();
  if (!this.state.networkPresent){ //chargement data local si pas de connection
    this._retrieveData();
  } else if(this.state.networkPresent) {
    return fetch('http://192.168.1.65/aspen/ws.php?action=getAdherents&token='.concat(this.state.token))
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({
        isLoading: false,
        adherents: responseJson,
        adherentsFiltres: responseJson,
      }, function(){
      });
      this._storeData();

    })
    .catch((error) =>{
      console.log(error);
      if(this.state.networkPresent) {
        //chargement data local si reseau ok mais pb acces
        this._retrieveData();
      }
      this.setState({
        isLoading: false,
        }, function(){
        });

    });
  }
}

onChangeSection = (newSection) => {
  let adhs=[];

  if(newSection==="toutes"){
    adhs = this.state.adherents
  }else{
    let i = 0
    for (let index = 0; index < this.state.adherents.length; index++) {
      const adh = this.state.adherents[index];
      if(adh.section === newSection){
        adhs[i] = adh;
        i++;
      }
    }
  }
  this.setState({
    adherentsFiltres:adhs,
    section: newSection});
}



onPressLogin= () => {
  const value = this._form.getValue(); // use that ref to get the form value

    
  let url = 'http://192.168.1.65/aspen/ws.php?action=login&login='.concat(value.login).concat('&pass=').concat(value.password);
  return fetch(url)
  .then((response) => response.json())
  .then((responseJson) => {
    if(responseJson.status === "logged"){
      this.setState({
        token: responseJson.token,
        logged:true
      }, function(){
      });
    }else{
      console.log('rate');
    }
    /*
    this.setState({
      isLoading: false,
      adherents: responseJson,
      adherentsFiltres: responseJson,
    }, function(){
    });
    this._storeData();
*/
  })
  .catch((error) =>{
    console.log(error);
    /*
    this.setState({
      isLoading: false,
      }, function(){
      });
      */
  });
  

}
  render(){
    if(this.state.isLoading){
      return(
        <View style={styles.container}>
          <ActivityIndicator/>
        </View>
      )
    }
    if(this.state.networkPresent && !this.state.logged){
      return(
        <View style={styles.container}> 
        <Form 
          ref={c => this._form = c} // assign a ref
          type={User} 
          options={options}
          />
        <Button
          title="Sign Up!"
          onPress={this.onPressLogin}
        />
    </View>
      );
    }else{ 
      return(
        <View style={styles.container}>
          <Text>Choisir votre section</Text>
          <Picker
            selectedValue={this.state.section}
            style={styles.picker}
            onValueChange={(itemValue, itemIndex) => this.onChangeSection(itemValue)}>
            <Picker.Item label="Toutes" value="toutes" />
            <Picker.Item label="Volley-ball" value="volley" />
            <Picker.Item label="Ultimate freesby" value="ultimate" />
          </Picker>
          <MyFlatList data={this.state.adherentsFiltres} />
          
        </View>
      );
    }
  
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:20,
  },
  picker:{ 
    height: 60, 
    width: 200,
    backgroundColor:'#A9D0F5',
  }
});